import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="remote_control_linux_server-flavioasilva2",
    version="0.0.1",
    author="Flávio Amaral e Silva",
    author_email="flavio.asilva@gmail.com",
    description="The RemoteControl linux server.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/remotecontrolnwq0/remote_control_linux_server",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX :: Linux",
        "Topic :: System :: Operating System",
        "Topic :: System :: Systems Administration",
        "Topic :: Utilities"
    ]
)