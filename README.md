# RemoteControl: Linux Server

This app is a set of python modules that can be combined with each other to control a linux workstation from an Android phone.

The main module is the remote_control_linux_server.

Then, the user session control and the clipboard control modules will be implemented. A discovery and communication protocol will also be implemented.